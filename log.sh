#!/bin/bash
while true
do
	file="./load.log"
	if [ -f "$file" ]
	then
		cat /proc/loadavg 2>&1 | sed "s/^/`date '+ %y-%m-%d--%H:%M:%S'` /" >> $file
	else
		echo "ts	1min	5min	15min	executing/scheduling	lastPID" > $file
	fi
	file="./mem.log"
	if [ -f "$file" ]
	then
		free | grep Mem: 2>&1 | sed "s/^/`date '+ %y-%m-%d--%H:%M:%S'` /" >> $file
	else
		echo "ts	desc	 total       used       free     shared    buffers     cached" > $file
	fi
	file="./swap.log"
	if [ -f "$file" ]
	then
		free | grep Swap: 2>&1 | sed "s/^/`date '+ %y-%m-%d--%H:%M:%S'` /" >> $file
	else
		echo "ts	desc	 total       used       free" > $file
	fi
	file="./disk.log"
	if [ -f "$file" ]
	then
		disk=$(cat /sys/block/mmcblk0/stat)
		words=( $disk )
		echo $(( ${words[3]} / ${words[0]}))       $((  ${words[7]} / ${words[4]}))	${words[8]} 2>&1 | sed "s/^/`date '+ %y-%m-%d--%H:%M:%S'` /" >> $file
	else
		echo "ts	readAvg	writeAvg	queue" > $file
	fi
	sleep 1
done
